#include "../inc/nrf.h"

/*		nRF24L01+ Library, file nrf.cpp
	Author: Sky-WaLkeR (aka shadowalker)
	  Date: 11.06.2013 21:56
	Tested: ATmega8 @ 8Mhz

	Description:
		For description see file nrf.h

	Dependencies (included in nrf.h):
		inc/nrf.h
		inc/nrf_defs.h
		inc/libSPI.h
		lib/libSPI.cpp

	Global dependencies (included in nrf.h):
		avr/io.h
		util/delay.h
*/

uint8_t _nrf_mode;

/************* FUNCTION *************\
 	  Syntax: nrf_init ( uint8_t mode = NRF_MODE_RX )
 Description: inits nRF module, sets settings to default
			   Must be called before all work with module
  Parameters: mode - can be NRF_MODE_RX, NRF_MODE_TX and
			   NRF_MODE_PWRDOWN
 	  Return: nothing
*/
void nrf_init(uint8_t mode) {
	NRF_DDR |= (1<<NRF_CSN)|(1<<NRF_CE);
	nrf_CE_lo;
	nrf_CSN_hi;

	nrf_set_ch(nrf_def_ch);
	nrf_write_byte(RX_PW_P0, nrf_def_payload);

	switch (mode){
		case NRF_MODE_PWRDOWN:
			nrf_mode_powerdown(); break;
		case NRF_MODE_TX:
			nrf_mode_tx(); break;
		default:
			nrf_mode_rx(); break;
	}
}


/************* FUNCTION *************\
 	  Syntax: nrf_init_ex ( uint8_t mode = NRF_MODE_RX )
 Description: Same as nrf_init, but this func turns on Enchanced 
			   ShockBurst and dynamic payload
  Parameters: mode - can be NRF_MODE_RX, NRF_MODE_TX and
			   NRF_MODE_PWRDOWN
 	  Return: nothing
*/
void nrf_init_ex(uint8_t mode){
	nrf_init(mode);
	nrf_en_aa_on();
	nrf_dynamic_payload_on();
}


/************* FUNCTION *************\
 	  Syntax: nrf_mode_rx ()
 Description: Sets nRF mode to receive mode
  Parameters: nothing
 	  Return: nothing
*/
void nrf_mode_rx(){
	nrf_write_byte(CONFIG, nrf_get_config() | 1<<PWR_UP | 1<<PRIM_RX);
	nrf_CE_hi;
	_nrf_mode=NRF_MODE_RX;
}


/************* FUNCTION *************\
 	  Syntax: nrf_mode_tx ()
 Description: Sets nRF mode to transmit mode
  Parameters: nothing
 	  Return: nothing
*/
void nrf_mode_tx(){
	nrf_write_byte(CONFIG, (nrf_get_config() | 1<<PWR_UP ) & ~(1<<PRIM_RX) );
	nrf_CE_hi;
	_nrf_mode=NRF_MODE_TX;
}


/************* FUNCTION *************\
 	  Syntax: nrf_mode_powerdown ()
 Description: Sets nRF mode to powerdown mode
  Parameters: nothing
 	  Return: nothing
*/
void nrf_mode_powerdown(){
	nrf_write_byte(CONFIG, (nrf_get_config() & ~(1<<PRIM_RX | 1<<PWR_UP) ));
	nrf_CE_lo;
}

/************* FUNCTION *************\
 	  Syntax: nrf_set_addr_rx(uint8_t * addr)
 Description: Sets RX address (pipe0)
  Parameters: addr - address array (5 bytes, LSB first)
 	  Return: nothing
*/
void nrf_set_addr_rx(uint8_t * addr) {
	nrf_CE_lo;
	nrf_write_bytes(RX_ADDR_P0, addr, 5);
	if (_nrf_mode!=NRF_MODE_PWRDOWN) nrf_CE_hi;
}


/************* FUNCTION *************\
 	  Syntax: nrf_set_addr_tx(uint8_t * addr)
 Description: Sets TX address
  Parameters: addr - address array (5 bytes, LSB first)
 	  Return: nothing
*/
void nrf_set_addr_tx(uint8_t * addr) {
	nrf_CE_lo;
	nrf_write_bytes(TX_ADDR, addr, 5);
}


/************* FUNCTION *************\
 	  Syntax: nrf_set_ch(uint8_t ch)
 Description: Sets channel
  Parameters: ch - channel (1 to 127)
 	  Return: nothing
*/
void nrf_set_ch(uint8_t ch){
	nrf_CE_lo;
	nrf_write_byte(RF_CH, ch);
	if (_nrf_mode!=NRF_MODE_PWRDOWN) nrf_CE_hi;
}


/************* FUNCTION *************\
 	  Syntax: nrf_set_retr( uint8_t count, uint8_t delay)
 Description: Sets retransmit options
  Parameters: count - count of tries (0 to 15)
			  delay - from _250uS to _4000uS, step _250uS (use this macroses,
			   like _1250uS, _2750uS, _500uS 
 	  Return: nothing
*/
void nrf_set_retr( uint8_t count, uint8_t delay){
	nrf_CE_lo;
	if(count>15||delay>15) return;
	nrf_write_byte(SETUP_RETR, count|delay<<4);
	if (_nrf_mode!=NRF_MODE_PWRDOWN) nrf_CE_hi;
}


/************* FUNCTION *************\
 	  Syntax: nrf_set_speed(uint8_t spd)
 Description: Sets speed
  Parameters: spd - speed: 250 Kbit/s (SPD_250K)
						     1 Mbit/s (SPD_1M)
						     2 Mbit/s (SPD_2M) 
							(use only these macroses)
 	  Return: nothing
*/
void nrf_set_speed(uint8_t spd){
	nrf_CE_lo;
	uint8_t tmp=nrf_read_byte(RF_SETUP);
	switch (spd){
	  case SPD_250K:
		tmp|=1<<5; tmp&=~(1<<3); break;
	  case SPD_1M:
		tmp&=~(1<<5|1<<3); break;
	  case SPD_2M:
		tmp|=1<<3; tmp&=~(1<<5); break;
	}
	nrf_write_byte(RF_SETUP, tmp);
	if (_nrf_mode!=NRF_MODE_PWRDOWN) nrf_CE_hi;
}


/************* FUNCTION *************\
 	  Syntax: nrf_enable_pipes( uint8_t flag )
 Description: Enables selected pipes and disables other
  Parameters: flag - use macroses PIPE_0, PIPE_1..PIPE_5,
			   PIPE_ALL, PIPE_NONE
 	  Return: nothing
*/
void nrf_enable_pipes( uint8_t flag ){
    nrf_CE_lo;
    nrf_write_byte(EN_RXADDR, flag);
    nrf_CE_hi;
}


/************* FUNCTION *************\
 	  Syntax: nrf_set_addr_rx_p1(uint8_t * addr)
 Description: Sets RX address (pipe1)
  Parameters: addr - 5 bytes array, LSB first
 	  Return: nothing
*/
void nrf_set_addr_rx_p1(uint8_t * addr) {
    nrf_CE_lo;
    nrf_write_bytes(RX_ADDR_P1,addr,5);
    nrf_CE_hi;
}

/************* FUNCTION *************\
 	  Syntax: nrf_set_addr_rx_pipe(uint8_t p_num, uint8_t addr)
 Description: Sets LSB byte of address for selected pipe
  Parameters: p_num - pipe number (digit from 2 to 5, not macroses from enable_pipes func!)
			  addr - one byte of address (LSB)
 	  Return: nothing
	 Remarks: This is only first byte of address, bytes 1..4 is same with pipe1 address
*/
void nrf_set_addr_rx_pipe(uint8_t p_num, uint8_t addr){
    nrf_CE_lo;
    nrf_write_byte(RX_ADDR_P0+p_num, addr);
    nrf_CE_hi;
}


/************* FUNCTION *************\
 	  Syntax: nrf_send (uint8_t *data, uint8_t len )
 Description: Transmits packet. Non-blocking function.
  Parameters: data - payload array (contains data to transmit)
			  len  - length of payload 
 	  Return: nothing
	 Remarks: use only if you don't use Enchanced ShockBurst and
			   dynamic paylod length 
*/
void nrf_send(uint8_t *data, uint8_t len){
	nrf_CE_lo;

	nrf_write_byte(STATUS, 1<<MAX_RT | 1<<TX_DS);

	nrf_mode_tx();
	if (_nrf_mode==NRF_MODE_PWRDOWN) _delay_ms(4);
	_nrf_mode=NRF_MODE_TX;

	nrf_CSN_lo;
	spi_fast_shift( FLUSH_TX );
	nrf_CSN_hi;

	nrf_CSN_lo;
	spi_fast_shift( W_TX_PAYLOAD );
	spi_transmit_sync(data,len);
	nrf_CSN_hi;

	nrf_CE_hi;
}

/************* FUNCTION *************\
 	  Syntax: nrf_send_ex (uint8_t *data, uint8_t len )
 Description: Transmits packet. Blocking function - waits for 
			   transmit acknowledgement or failure
  Parameters: data - payload array (contains data to transmit)
			  len  - length of payload 
 	  Return: status of transmit
			   NRF_SEND_SUCCESS - data send, ack received
			   NRF_SEND_FAILURE - data sent, but no ack (rx don't received)
			   NRF_SEND_TIMEOUT - rare error, data didn't sent
	 Remarks: use only if you use Enchanced ShockBurst and
			   dynamic paylod length
*/
uint8_t nrf_send_ex(uint8_t *data, uint8_t len, uint8_t autoack){
	nrf_CE_lo;

	nrf_mode_tx();
	if (_nrf_mode==NRF_MODE_PWRDOWN) _delay_ms(4);
	_nrf_mode=NRF_MODE_TX;

	nrf_CSN_lo;
	spi_fast_shift( FLUSH_TX ); // Write cmd to flush tx fifo
	nrf_CSN_hi;

	nrf_CSN_lo;
	spi_fast_shift( autoack==0? W_TX_PAYLOAD_NOACK : W_TX_PAYLOAD ); // Write cmd to write payload
	spi_transmit_sync(data,len);	// Write payload
	nrf_CSN_hi;

	nrf_CE_hi;
	uint8_t i=0;
	while ((nrf_get_status() & (1<<MAX_RT | 1<<TX_DS)) == 0){
		_delay_ms(2);
		if (++i==15) return NRF_SEND_TIMEOUT;
	}

	uint8_t status=nrf_get_status();
	nrf_CE_lo;
	nrf_mode_rx(); _nrf_mode=NRF_MODE_RX;

	nrf_write_byte(STATUS, 1<<MAX_RT | 1<<TX_DS);

	if ((status & (1<<MAX_RT))>>MAX_RT) {
		return NRF_SEND_FAILURE;
	} else {
		return NRF_SEND_SUCCESS;
	}
}


/************* FUNCTION *************\
 	  Syntax: nrf_send_ex (uint8_t *data )
 Description: Another version of nrf_send_ex - length of payload
			   choosed automatically. First NULL ('\0') symbol - end of string
  Parameters: data - payload array (contains data to transmit)
			  len  - length of payload 
 	  Return: status of transmit
			   NRF_SEND_SUCCESS  - data send, ack received
			   NRF_SEND_FAILURE  - data sent, but no ack (rx don't received)
			   NRF_SEND_TIMEOUT  - rare error, data didn't sent
			   NRF_SEND_TOO_LONG - payload sie is bigger than 32 bytes
	 Remarks: use only if you use Enchanced ShockBurst and
			   dynamic paylod length
*/
uint8_t nrf_send_ex(uint8_t *data){
	uint8_t ret, len, i=0;
	while (1){
		if (data[i++]==0) {
			len=i;
			break;
		}
		if (i>32) return NRF_SEND_TOO_LONG;
	}
	ret=nrf_send_ex(data, len);
	return ret;
}

/************* FUNCTION *************\
 	  Syntax: nrf_data_ready()
 Description: Checks RX FIFO for unread packets
  Parameters: nothing
 	  Return: 1 - data received and ready for read
	 Remarks: don't call this func very microsecond ;)
			  If module not in RX mode - returns 0 
*/
uint8_t nrf_data_ready() {
	if (_nrf_mode!=NRF_MODE_RX) return 0;
	return nrf_get_status() & (1<<RX_DR);
}


/************* FUNCTION *************\
 	  Syntax: nrf_get_data (uint8_t *data, uint8_t len, uint8_t *p_num=0)
 Description: reads packet from RX FIFO
  Parameters: data   - payload array (will contain received data
			  len    - payload length
			  p_num  - number of pipe
 	  Return: nothing
	 Remarks: use only if you don't use Enchanced ShockBurst and
			   dynamic paylod length
			  You may ignore third parameter
*/
void nrf_get_data(uint8_t *data, uint8_t len, uint8_t *p_num){
	nrf_CSN_lo;
	*p_num = (spi_fast_shift(R_RX_PAYLOAD) & 0b1110)>>1;
	spi_transfer_sync(data,data,len);
	nrf_CSN_hi;

	nrf_write_byte(STATUS, 1<<RX_DR);
}


/************* FUNCTION *************\
 	  Syntax: nrf_get_data_ex (uint8_t *data, uint8_t *p_num=0)
 Description: reads packet from RX FIFO and returns it's size
			   Version for EN_AA and dynamic payload
  Parameters: data   - payload array (will contain received data
			  p_num  - number of pipe
 	  Return: nothing
	 Remarks: use only if you use Enchanced ShockBurst and
			   dynamic paylod length
			  You may ignore second parameter
*/
uint8_t nrf_get_data_ex(uint8_t * data, uint8_t *p_num){
	uint8_t payload_len;

	nrf_CSN_lo;
	*p_num = (spi_fast_shift(R_RX_PL_WID) & 0b1110)>>1;
	payload_len = spi_fast_shift(NOP);
	nrf_CSN_hi;

	nrf_CSN_lo;
	spi_fast_shift( R_RX_PAYLOAD );
	spi_transfer_sync(data,data,payload_len);
	nrf_CSN_hi;

	nrf_write_byte(STATUS, 1<<RX_DR);
	return payload_len;
}


/************* FUNCTION *************\
 	  Syntax: nrf_en_aa_on()
 Description: Enables Enchanced ShockBurst mode
  Parameters: nothing
 	  Return: nothing
*/
void nrf_en_aa_on(){
	nrf_write_byte(EN_AA, 0b111111);
}


/************* FUNCTION *************\
 	  Syntax: nrf_en_aa_off)
 Description: Disables Enchanced ShockBurst mode
  Parameters: nothing
 	  Return: nothing
*/
void nrf_en_aa_off(){
	nrf_write_byte(EN_AA, 0);
}


/************* FUNCTION *************\
 	  Syntax: nrf_dynamic_payload_on()
 Description: Enables dynamic payload mode
  Parameters: nothing
 	  Return: nothing
*/
void nrf_dynamic_payload_on(){
	nrf_write_byte(FEATURE, 1 << EN_DPL);
	nrf_write_byte(DYNPD, 0b111111);
}


/************* FUNCTION *************\
 	  Syntax: nrf_dynamic_payload_off()
 Description: Disables dynamic payload mode
  Parameters: nothing
 	  Return: nothing
*/
void nrf_dynamic_payload_off(){
	nrf_write_byte(FEATURE, 0);
	nrf_write_byte(DYNPD, 0);
}


/*--------- INTERNAL USE ONLY ---------*\
 	  Syntax: nrf_read_bytes(uint8_t reg, uint8_t * value, uint8_t len)
 Description: Reads array of data from nRF register
  Parameters: reg   - address of register
			  value - array (will contain data)
			  len   - length of data (bytes)
 	  Return: nothing
*/
void nrf_read_bytes(uint8_t reg, uint8_t * value, uint8_t len){
	nrf_CSN_lo;
	spi_fast_shift(R_REGISTER | (REGISTER_MASK & reg));
	spi_transfer_sync(value, value, len);
	nrf_CSN_hi;
}


/*--------- INTERNAL USE ONLY ---------*\
 	  Syntax: nrf_read_byte(uint8_t reg)
 Description: Reads one byte of data from nRF register
  Parameters: reg   - address of register
 	  Return: data
*/
uint8_t nrf_read_byte(uint8_t reg){
	uint8_t ret;
	nrf_CSN_lo;
	spi_fast_shift(R_REGISTER | (REGISTER_MASK & reg));
	ret=spi_fast_shift(reg);
	nrf_CSN_hi;
	return ret;
}


/*--------- INTERNAL USE ONLY ---------*\
 	  Syntax: nrf_write_bytes(uint8_t reg, uint8_t * value, uint8_t len)
 Description: Writes array of data to nRF register
  Parameters: reg   - address of register
			  value - array of data
			  len   - length of data (bytes)
 	  Return: nothing
*/
void nrf_write_bytes(uint8_t reg, uint8_t * value, uint8_t len) {
	nrf_CSN_lo;
	spi_fast_shift(W_REGISTER | (REGISTER_MASK & reg));
	spi_transmit_sync(value, len);
	nrf_CSN_hi;
}


/*--------- INTERNAL USE ONLY ---------*\
 	  Syntax: nrf_write_byte(uint8_t reg, uint8_t * value)
 Description: Writes one byte of data to nRF register
  Parameters: reg   - address of register
			  value - data to write
 	  Return: nothing
*/
void nrf_write_byte(uint8_t reg, uint8_t value){
	nrf_CSN_lo;
	spi_fast_shift(W_REGISTER | (REGISTER_MASK & reg));
	spi_fast_shift(value);
	nrf_CSN_hi;
}

/*--------- INTERNAL USE ONLY ---------*\
 	  Syntax: nrf_get_status()
 Description: Returns value of STATUS register
  Parameters: nothing
 	  Return: value of STATUS register
*/
uint8_t nrf_get_status(){
	uint8_t status;
	nrf_CSN_lo;
	status = spi_fast_shift(NOP);
	nrf_CSN_hi;
	return status;
}


/*--------- INTERNAL USE ONLY ---------*\
 	  Syntax: nrf_get_config()
 Description: Returns value of CONFIG register
  Parameters: nothing
 	  Return: value of CONFIG register
*/
uint8_t nrf_get_config(){
	return nrf_read_byte(CONFIG);
}
